package api.db.sqlite.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sqlite.JDBC;

import api.domain.User;

public class DatabaseManager{
	Connection conn;
	public DatabaseManager(String pathDB){
		super();
		this.conn = connect(pathDB);
	}
	
	private Connection connect(String pathDB){
		Connection conn = null;
		try {
		    String url = "jdbc:sqlite:"+pathDB;
		    DriverManager.registerDriver(new JDBC());
	        conn = DriverManager.getConnection(url);
		} catch ( SQLException ex) {
		     System.err.println(ex);
		}
		return conn;
	}
	
	public void close() {
		try {
            conn.close();
        } catch (SQLException ex) {
            System.out.println("No s'ha pogut tancar la conexi�");
        }
	}
	
	public void showUsers(){
        ResultSet result = null;
        try {
            PreparedStatement st = conn.prepareStatement("select * from users");
            result = st.executeQuery();
            while (result.next()) {
                System.out.print("ID: ");
                System.out.println(result.getInt("id"));
 
                System.out.print("Nombre: ");
                System.out.println(result.getString("name"));
 
                System.out.print("rol: ");
                System.out.println(result.getString("role"));
                System.out.println("=======================");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
	
	public User getUser(String id) {
		User user = new User();
		ResultSet result = null;
        try {
            PreparedStatement st = conn.prepareStatement(
            		"select * from users where ID = "+ id +";");
            result = st.executeQuery();
            if (result.next()) {
            	user.setId(result.getInt("id"));
            	user.setId_subject(result.getInt("id_subject"));
            	user.setName(result.getString("name"));
            	user.setPassword(result.getString("password"));
            	user.setRole(result.getString("role"));
            }else {
            	user = null;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
		return user;
	}
}
