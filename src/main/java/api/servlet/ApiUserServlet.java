package api.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;


import api.db.sqlite.manager.DatabaseManager;
import api.domain.User;
import api.forum.utils.ApiUtils;

/**
 * Servlet implementation class ApiUserServlet
 */
public class ApiUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	api.db.sqlite.manager.DatabaseManager DBmanager;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApiUserServlet() {
        super();
    }

    @Override
	public void init() {
		DBmanager = new DatabaseManager(ApiUtils.urlDBSqlite);
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = new User();
		String Token = request.getHeader("Authorization").substring(7);		
		DecodedJWT jwt = JWT.decode(Token);
		Claim id = jwt.getClaim("internId");
		user = DBmanager.getUser(id.asString());
		if(user != null) {
			String json = new Gson().toJson(user);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}else {
			/**
			 * retornar un error d'autenticació
			 */
			response.sendError(403, "Usuari no trobat");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
