package Oauthservlet.verifier;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import javax.servlet.http.HttpFilter;

@SuppressWarnings("serial")
public class VerifierFilter extends HttpFilter {

	public void doFilter(HttpServletRequest servletRequest, HttpServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		try {
			String token = servletRequest.getHeader("Authorization");
			JWTVerifier verifier = JWT.require(Algorithm.HMAC256("secret"))
					.withIssuer("oauth")
					.build();
			verifier.verify(token);
		} catch (JWTVerificationException exception) {
			servletResponse.sendError(401, "Token incorrecte");
		}
		// pass the request along the filter chain
		chain.doFilter(servletRequest, servletResponse);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}