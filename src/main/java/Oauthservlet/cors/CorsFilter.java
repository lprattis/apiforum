package Oauthservlet.cors;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpFilter;

@SuppressWarnings("serial")
public class CorsFilter extends HttpFilter {
 
    public void doFilter(HttpServletRequest servletRequest, HttpServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        servletResponse.addHeader("Access-Control-Allow-Origin", "*");
        servletResponse.addHeader("Access-Control-Allow-Methods","GET, POST");
        servletResponse.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept"); 
        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
//        if (servletRequest.getMethod().equals("OPTIONS")) {
//        	servletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
//            return;
//        }
 
        // pass the request along the filter chain
          chain.doFilter(servletRequest, servletResponse);
    }

    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }
 
}